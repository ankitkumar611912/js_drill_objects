function values(obj) {
    if (typeof obj !== 'object' || obj === null) {
        return [];
    }
    let values = [];
    for( key in obj){
        values.push(obj[key]);
    }
    return values;
}
module.exports = values;