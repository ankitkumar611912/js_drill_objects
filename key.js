function keys(obj) {
    if (typeof obj !== 'object' || obj === null) {
        return [];
    }
    let keys = [];
    for( key in obj){
        keys.push(key);
    }
    return keys;
}
module.exports = keys;