function defaults(obj, defaultProps) {
    if(typeof obj !== 'object' || obj === null){
        return {};
    }
    Object.keys(defaultProps).forEach(key => {
        if (obj[key] === undefined) {
            obj[key] = defaultProps[key];
        }
    });
    return obj;
}
module.exports = defaults;