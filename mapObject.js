function transform(value) {
    if (typeof value === 'number') {
        return value * 2;
    } else if (typeof value === 'string') {
        return value.toUpperCase();
    }
    return value;
}
function mapObject(obj, transform) {
    if (typeof obj !== 'object' || obj === null) {
        return {};
    }
    for(let key in obj){
        obj[key] = transform(obj[key]);
    }
    return obj;
}
module.exports = {
    mapObject,
    transform
};
