function invert(obj) {
    const result = {};
    Object.keys(obj).forEach(key => {
        result[obj[key]] = key;
    });
    return result;
}
module.exports = invert;