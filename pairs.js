
function pairs(obj){
    if (typeof obj !== 'object' || obj === null) {
        return [];
    }
    let pair = [];
    for(let key in obj){
        let values = [];
        values.push(key);
        values.push(obj[key]);
        pair.push(values);
    }
    return pair;
}

module.exports  = pairs;